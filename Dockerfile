FROM quay.io/ansible/ansible-runner:latest

RUN pip config set global.index-url https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple && \
    pip config set global.trusted-host artifactory.esss.lu.se
 