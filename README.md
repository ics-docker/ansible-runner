Ansible Runner
========

ESS specific [Ansible Runner](https://github.com/ansible/ansible-runner) container image.

This image is configured to use ESS internal pypi index.
